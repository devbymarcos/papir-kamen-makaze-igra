//Globalne promenljive
let score = sessionStorage.getItem('globalScore') || 0;
let scoreDOM = document.querySelector('span.score-number');
scoreDOM.innerText = score;
let odabirKorisnika = '';
let odabirRacunara = '';
let checker = false;
let checker2 = false;

//Ako je sessionStorage neka izmenjena vrednost onda uzmi nju, ako nije onda score stavi na 0

//Prikaz pravila - Rules
let rules = (function test(){
    /* Na ovaj način dobio sam samopozivjauću funkciju koju mogu kasnije ponovo pozvati, doduše to mi u ovom slučaju neće trebati ali svakako ovakvo navođenje imitira kapsuliranje */

    let rulesCont = document.querySelector('div.rules-cont');
    let rulesCloseButton = document.querySelector('img.rules-cont-close').addEventListener('click',rulesOff);
    let rulesShowButton = document.querySelector('button.rules').addEventListener('click',rulesShow);

        /* Automatic shout down rules div */
    const rulesContTimer = setTimeout(rulesOff,500);

    function rulesOff(){
        /* If user click on close button, clear Timer */
        clearTimeout(rulesContTimer);
        /* Remove old class */
        rulesCont.classList.remove(rulesCont.classList[1]);
        /* Add new class */
        rulesCont.classList.add('displayOff');
    };

    function rulesShow(){
        /* Remove old class */
        rulesCont.classList.remove(rulesCont.classList[1]);
        /* Add new class */
        rulesCont.classList.add('displayFlex');
    }

        /* Bilo bi dobro ako bih uspeo da ceo blok koda za Rules, stavim u jedan objekat koji ce imati svoj funkcije kao sto ih ima ovaj blok koda. Na taj nacin bih dobio enkapsuliranje odnosno to da mi sve varijable bivaju na jedno zatvorenom mestu. */

    return test;
}());

//Odabir korisnika

let step1 = (function start(){
    //DEBUGGING
    console.log('Funkcija step1()');

    (function checkOtherElements(){
           /* Treba mi funkcija koja ce da proveri da li su svi elementi pod klasom displayOff, ako nisu staviti ih pod tu klasu, 
           i proveriti da li je klasa step1 prikazana, ako nije prikazati je */
        let step1Div = document.querySelector('.step1-circles');
        let step2Div = document.querySelector('.step2-circles');

        /* Remove class for show step2*/
        step2Div.classList.remove('displayGrid');
        step2Div.classList.add('displayOff');

        /* Poništiti stilizovanje step2 za prikazivanje rezultata */
        step2Div.classList.remove('resultWrite');

        /* Ukloniti rezultat koji se prikazuje */
        let resultCont = document.querySelectorAll('.result-container div');
        resultCont[0].classList.add('displayOff');
        resultCont[0].classList.remove('displayFlex');
        resultCont[1].classList.add('displayOff');
        resultCont[1].classList.remove('displayFlex');

        /* Prikazati step1 */
        step1Div.classList.add('displayGrid');
        step1Div.classList.remove('displayOff');

        /* Poništavanje odabira korisnika */
        odabirKorisnika = '';
        odabirRacunara = '';

        /* Ponistavanje u stilizaciji step2 krugova*/
        let step2CircleUser = document.querySelector('.step2-circles .circle-point-user');
            /* Brisanje klasa svih sem osnovne */
        step2CircleUser.className = 'circle-point-user';
        let step2CirclePc = document.querySelector('.step2-circles .circle-point-pc');
        /* Brisanje klasa svih sem osnovne */
        step2CirclePc.className = 'circle-point-pc';
    }());
    
    (function listener(){
        /* Ova funkcija je dodata jer je bio BUG taj da se svaki put po izvrsavanju funkcije step1 dodeli novi listener na sve krugove, to smo resili tako sto sam stavio globalnu varijablu(checker) koja proverava da li je dodeljen listener */
        if(checker == false){

            /* Targetiranje svih krugova */
        let klikKorisnika = document.querySelectorAll('.step1-circles .circle-point div');

        /* Listener na sve krugove posebno */
        for(let i of klikKorisnika){
            i.addEventListener('click',unos);
        }
        checker = true;
        }else{
            return;
        }
    }());
    

    /* Akcija nakon klika */
    function unos(e){
        //DEBUGGING
        console.log('Funkcija step1() - unos()');

        /* Određivanje klase unosa */
        let klaseUnosa = (e.target.classList[1]).split('-')[2];

        /* Definisanje odabira korisnika u globalnoj varijabli */
        odabirKorisnika = klaseUnosa;

        /* Pozivanje sledećeg koraka i prelazak na sledeću funkciju*/
        step2(e.target.classList[1]);
        return;

    }
    


    return start;
}());



//Čekanje odgovora računara
let step2 = function mid(e){
    //DEBUGGING
    console.log('Funkcija step2()');

    let step2Div = document.querySelector('.step2-circles')
    let step2CircleUser = document.querySelector('.step2-circles .circle-point-user');
    let step2CirclePc = document.querySelector('.step2-circles .circle-point-pc');


    /* Uklanjanje step1 */
    let step1Div = document.querySelector('.step1-circles');
    step1Div.classList.add('displayOff');
    step1Div.classList.remove('displayGrid');


    /* Dodeljivanje ispravne klase na levi krug i prikazivanje step2*/
    step2CircleUser.classList.add(e);

    step2Div.classList.add('displayGrid');
    step2Div.classList.remove('displayOff');





    /* Odabir racunara neke od dve opcije */
    let pcChooseArr = ['paper','scissors','rock'];
            /* Ukloni ono sto je korisnik odabrao */
    pcChooseArr.splice(pcChooseArr.indexOf(odabirKorisnika),1);
    let randomNumber = Math.floor(Math.random()*pcChooseArr.length);
    
    odabirRacunara = pcChooseArr[randomNumber];
        /* Dodeljivanje klase krugu PC-a */
    setTimeout(()=>{
        step2CirclePc.classList.add(`circle-point-${odabirRacunara}`);
    },500)
    
    

    /* Ko je pobedio?! */
    let ishod = undefined;
    switch(odabirKorisnika) {
        case 'paper': 
            if(odabirRacunara == 'scissors'){
                ishod = false;
            }else{
                ishod = true;
            }
            break;
        case 'scissors':
            if(odabirRacunara == 'rock'){
                ishod = false;
            }else{
                ishod = true;
            }
            break;
        case 'rock':
            if(odabirRacunara == 'paper'){
                ishod = false;
            }else{
                ishod = true;
            }
            break;
    }

    /* Ispisivanje rezultata na ekranu i dodeljivanje skora */
    setTimeout(()=>{
        if(ishod)/* TRUE */{
            /* Ispisivanje */
            let resultCont1 = document.querySelector('.result-text-win');
            resultCont1.classList.add('displayFlex');
            resultCont1.classList.remove('displayOff');

            /* Dodavanje skora */
            score ++;
            scoreDOM.innerText = score;
            sessionStorage.setItem('globalScore',score);


        }else/* FALSE */{
            /* Ispisivanje */
            let resultCont2 = document.querySelector('.result-text-lose');
            resultCont2.classList.add('displayFlex');
            resultCont2.classList.remove('displayOff');

            /* Oduzimanje skora */
            score --;
            scoreDOM.innerText = score;
            sessionStorage.setItem('globalScore',score);
        }

    },500);


    /* Ako korisnik klikne na dugme PlayAgain pali se step1 */
    (function playAgainListener(){
        if(!checker2){
            let playAgain = document.querySelectorAll('button.play-again');
            for(let i of playAgain){
                i.addEventListener('click',playAgainFun);
            }
            checker2 = true
        }else{
            return;
        }
    }());
    

    function playAgainFun(){
        console.log('playagain funkcija');
        step1();
    }

    return mid;
};



//Ispis da li je kornisk pobedio ili izgubio partiju

//Dodeljivanje/Oduzimanje bodova

//PlayAgain aktivira funkciju koja skriva sve sem Step1, i ukida sve do tada dodeljene klase kako bi program bio spreman za naredni krug

